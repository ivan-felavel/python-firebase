El robot NAO y Firebase
###################################



Firebase Realtime Database en el robot NAO
===================================================


El robot es una plataforma con recursos muy limitados, no hay un SDK
específicamente hecho para el robot. Por lo anterior, se utilza la API REST
de Firebase Realtime Database para envíar y recibir información a través de
internet. Sin embargo, a pesar de ser una API REST, fue necesaria la creación
de un módulo escrito en Python que sirviera como una interfaz de alto nivel,
donde hacer una petición HTTP fuera igual de sencillo que llamar una función en
los SDK disponibles.

Primero se describe la estructura de éste módulo y a continuación la integración
con módulos de NAOqi para que el robot pueda enviar información de sus estado
y recibir datos que puedan ser interpretados como un comando.

API REST de Firebase
-----------------------------------

Se puede utilizar cualquier URL de la base de datos de Firebase como un endpoint
REST. Todo lo que se necesita es añadir la extensión ``.json`` al final del
URL y enviar una petición desde cualquier cliente HTTP.


Para poder utilizar la API REST, no está de más mostrar el mapeo de los métodos
HTTP y las operaciones sobre Firebase Realtime Database.

- **GET**, para leer información previamente almacenada en la base de datos.
- **PUT**, para escribir datos sobre una ubicación de la base de datos. Si existen datos los sobreescribe. Equivalente a ``set()`` de los SDK.
- **POST**, para añadir información en la base de datos. Se asigna un identificador único generado por Firebase. Es equivalente al ``push()`` de los SDK.
- **PATCH**, para actualizar datos en una ubicación sin sobreescribir. Equivalente a ``update()`` de los SDK.
- **DELETE**, elimina los datos en la ubicación dada.

La lista anterior muestra los métodos de lectura y escritura sobre la base
de datos, sin embargo, es indispensable que el robot reciba actualizaciones
en tiempo real también, esto para la parte del control remoto.
Los endpoints de la API REST soportan el protocolo **Server-sent events (SSE)**,
que define una API para recibir notificaciones push desde un servidor a través
de una conexión HTTP.

Para recibir las actualizaciones sobre una ubicación en la base de datos se
necesitan hacer tres cosas:

- Configurar el header ``Accept`` igual a ``text/event-stream``. Del lado del cliente.
- Respetar los rediccioramientos HTTP.
- Si la ubicación de la base de datos requiere permisos, incluit el parámetro ``auth``.

Módulo auxiliar para la API REST
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Para utilizar la API REST de manera más simple se desarrolló una biblioteca
que encapsulara los métodos de lectura, escritura y el streaming de datos.

Es un módulo de Python, compuesto por dos partes principales; la primera parte
es aquella que maneja las peticiones de escritura y lectura usando los métodos
HTTP antes mencionados, la segunda parte es la encargada de recibir las
notificaciones de parte del servidor. Para la primera el uso de la biblioteca
``requests`` es suficiente. Para la segunda, se emplean las bibliotecas
``threading``, para que en un hilo se escuchen los cambios, ``sseclient``,
un cliente del protocolo **SSE**, y ``socket``, el hilo del clente **SSE**.

A continuación se describe cada componente del módulo ``firebase``.

.. automodule:: firebase
  :members:

Módulo para ejecutar funciones de NAOqi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Módulo para conectar la API REST con NAOQi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. CloudNAO robot documentation master file, created by
   sphinx-quickstart on Wed Feb 28 23:30:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CloudNAO robot's documentation!
==========================================

.. toctree::
   :caption: Contents:

   introduction
   nao_firebase



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

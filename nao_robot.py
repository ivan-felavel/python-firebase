# encoding:utf-8
"""
.. module:: nao_robot
   :platform: Unix, Windows
   :synopsis: Un módulo para establecer los módulos de NAOqi que se ocuparán para recibir datos o enviar información a Firebase.

.. moduleauthor:: Ivan Feliciano <ivan.felavel@gmail.com>

"""

import io
from PIL import Image
import base64
from naoqi import ALProxy
import almath

DATA_VALUES_FROM_ROBOT = {
    "rightUSSensorValue": "Device/SubDeviceList/US/Right/Sensor/Value",
    "leftUSSensorValue": "Device/SubDeviceList/US/Left/Sensor/Value",
    "rightFootTotalWeight": "rightFootTotalWeight",
    "RightBumperPressed": "RightBumperPressed",
    "leftFootTotalWeight": "leftFootTotalWeight",
    "ChestButtonPressed": "ChestButtonPressed",
    "RearTactilTouched": "RearTactilTouched",
    "leftFootContact": "leftFootContact",
    "LeftBumperPressed": "LeftBumperPressed",
    "footContact": "footContact",
    "FrontTactilTouched": "FrontTactilTouched",
    "BatteryChargeChanged": "BatteryChargeChanged",
    "PostureChanged": "PostureChanged",
    "rightFootContact": "rightFootContact",
    "MiddleTactilTouched": "MiddleTactilTouched",
    "GyrometerX": "Device/SubDeviceList/InertialSensor/GyrX/Sensor/Value",
    "GyrometerY": "Device/SubDeviceList/InertialSensor/GyrY/Sensor/Value",
    "AccelerometerX": "Device/SubDeviceList/InertialSensor/AccX/Sensor/Value",
    "AccelerometerY": "Device/SubDeviceList/InertialSensor/AccY/Sensor/Value",
    "AccelerometerZ": "Device/SubDeviceList/InertialSensor/AccZ/Sensor/Value",
    "TorsoAngleX": "Device/SubDeviceList/InertialSensor/AngleX/Sensor/Value",
    "TorsoAngleY": "Device/SubDeviceList/InertialSensor/AngleY/Sensor/Value"
}

VALID_MOVE_CONFIG = {
    'MaxStepFrequency', 'MaxStepTheta', 'MaxStepX', 'MaxStepY', 'StepHeight',
    'RightStepHeight', 'LeftStepHeight'
}


class Robot(object):
    """Una clase para encapsular todos los métodos del robot
    que van a recibir o enviar información a Firebase.

    Los atributos de clase y sus valores son los siguientes::

        cam_idx = 0
        resolution = 0
        color_space = 11
        fps = 10
        camera_subscriber = 'cameraSubscriber0001'
        sonar_subscriber = 'sonarSubscriber0001'
        move_config = [
            ['MaxStepFrequency', 0.5],
            ['MaxStepTheta', 0.349],
            ['MaxStepX', 0.04],
            ['MaxStepY', 0.14],
            ['StepHeight', 0.02]
        ]
    """
    cam_idx = 0
    resolution = 0
    color_space = 11
    fps = 10
    camera_subscriber = 'cameraSubscriber0001'
    sonar_subscriber = 'sonarSubscriber0001'
    move_config = [
        ['MaxStepFrequency', 0.5],
        ['MaxStepTheta', 0.349],
        ['MaxStepX', 0.04],
        ['MaxStepY', 0.14],
        ['StepHeight', 0.02]
    ]

    def __init__(self, ip_address, port):
        """El constructor de la clase crea los proxies a los módulos de NAOqi.
        Recibe la la dirección ip y el puerto del robot.
        """
        self.ip = ip_address
        self.port = port

        self.motion_proxy = ALProxy('ALMotion', ip_address, port)
        self.camera_proxy = ALProxy('ALVideoDevice', ip_address, port)
        self.memory_proxy = ALProxy('ALMemory', ip_address, port)
        self.sonar_proxy = ALProxy('ALSonar', ip_address, port)
        self.text_to_speech_proxy = ALProxy('ALTextToSpeech', ip_address, port)

    def move_robot(self, _x, _y, _theta):
        """Método para ejecutar hacer caminar utilizando ``moveToward`` de la
        API de NAOqi.

        :param _x: La velocidad (-1, 1) sobre el eje **X**
        :param _y: La velocidad (-1, 1) sobre el eje **Y**
        :param _theta: La velocidad (-1, 1) sobre el eje **Z**
        """
        self.motion_proxy.moveToward(_x, _y, _theta, self.move_config)

    def stop_movement(self):
        """Método para detener los movimientos del robot, llama a ``stopMove()``
        de ``ALMotion``.
        """
        self.motion_proxy.stopMove()

    def get_values_from_memory(self):
        """Obtiene valores de la memoria del robot y retorna un diccionario
        con sus llaves y valores.

        :return: Un diccionario con los valores de ``ALMemory``
        :type: dict
        """
        values_to_return = {}
        for key in DATA_VALUES_FROM_ROBOT:
            try:
                values_to_return[key] = \
                    self.memory_proxy.getData(DATA_VALUES_FROM_ROBOT[key])
            except RuntimeError:
                pass
        return values_to_return

    def get_battery_level(self):
        """Solicita el valor del nivel de la batería desde la memoria del robot
        y lo retorna

        :return: Un número con el nivel actual de la batería
        :type: int
        """
        return self.memory_proxy.getData(DATA_VALUES_FROM_ROBOT['BatteryChargeChanged'])

    def move_joint(self, joint_name, angle):
        """Mueve una articulación del robot con respecto a la posción inicial de una
        articulación. Primero activa la rigidez del motor que mueve la articulación, realiza el movimiento en dos
        segundos y luego desactiva la rigidez del motor.
        """
        self.motion_proxy.setStiffnesses(str(joint_name), 1.0)
        self.motion_proxy.angleInterpolation(str(joint_name), angle * almath.TO_RAD, 2.0, True)
        self.motion_proxy.setStiffnesses(str(joint_name), 0.0)

    def subscribe_to_camera(self):
        """Se suscribe a la cámara del robot, con los parámetros que se definieron
        en los atributos de la clase.
        """
        self.camera_subscriber = self.camera_proxy.subscribeCamera( \
            self.camera_subscriber, self.cam_idx, \
            self.resolution, self.color_space, self.fps)

    def unsubscribe_to_camera(self):
        """Da de baja al suscriptor de la cámara
        """
        self.camera_proxy.unsubscribe(self.camera_subscriber)

    def get_image_from_robot(self):
        """Obtiene la imagen del robot codificada en base64. Primero con
        ``getImageRemote()`` recibe el contenedor de la imagen, y luego,
        el arreglo de bytes de la imagen en el contenedor, se guarda en una
        estructura de ``PIL`` para que pueda convertirse en una cadena.

        :return: La imagen codificada en base64
        :type: str
        """
        nao_image = self.camera_proxy.getImageRemote(self.camera_subscriber);
        raw_bytes = io.BytesIO()
        if not nao_image:
            print("Image not received")
            return None
        else:
            image_string = str(bytearray(nao_image[6]))
            im = Image.frombuffer("RGB", (nao_image[0], nao_image[1]), image_string, "raw", "RGB", 0, 1)
            im.save(raw_bytes, format="PNG")
            raw_bytes.seek(0)
            img_str = base64.b64encode(raw_bytes.getvalue())
            return img_str

    def change_posture(self, posture):
        """ Cambia entre dos posturas del robot, **Stand** y **Crouch**.

        :param posture: El nombre de la postura del robot que se desea obtener
        """
        if posture == 'Stand':
            self.motion_proxy.wakeUp()
        else:
            self.motion_proxy.rest()

    def say_speech(self, text):
        """Ejecuta la función ``say`` de ``ALTextToSpeech``. Dice la cadena
        enviada como parámetro.

        :param text: El texto que debe decir el robot
        """
        self.text_to_speech_proxy.say(text.encode("utf-8"))

    def set_move_config(self, move_config_map):
        """Cambia los parámetros de caminado del robot. Recibe un diccionario
        con los nuevos parámetros y sus valores. Los procesa para ver si son válidos
        y cambiarlos.
        """
        print ("Move config")
        print (move_config_map)
        move_config = [[str(key), float(move_config_map[key])]
                       if key in VALID_MOVE_CONFIG else None for key in move_config_map]
        self.move_config = list(filter(lambda x: x is not None, move_config))

    def subscribe_to_sonar(self):
        """Se suscribe al sonar para que se actualicen los valores en la memoria
        con las distancias a obstáculos.
        """
        self.sonar_proxy.subscribe(self.sonar_subscriber)

    def unsubscribe_to_sonar(self):
        """Se apagan los sonares.
        """
        self.sonar_proxy.unsubscribe(self.sonar_subscriber)

# encoding:utf-8
"""
.. module:: fire_nao
   :platform: Unix, Windows
   :synopsis: Un módulo para integrar la API REST de Firebase con módulos de NAOqi.

.. moduleauthor:: Ivan Feliciano <ivan.felavel@gmail.com>

"""

import time
from nao_robot import Robot
from firebase import FirebaseDatabase
from config import config


#: La instancia del objeto Robot, para ejecutar funciones de NAOqi. Se incializa con valor nulo.
ROBOT = None

#: La referencia a la base de datos de Firebase
ROOT_DB_REF = FirebaseDatabase(config['databaseURL'])
#: Referencia a la ubicación de los comandos en la base de datos
REF_TO_COMMANDS = ROOT_DB_REF.child('commands/' + config['robotUID'])
#: Referencia donde se guardan los logs del robot en la base de datos
REF_TO_LOGS = ROOT_DB_REF.child('logs/' + config['robotUID'])
#: Referencia a la ubicación con el estado de la conexión del robot y el nivel de batería
REF_TO_STATUS = ROOT_DB_REF.child('robots/' + config['robotUID'] + '/robotStatus')
#: Referencia a la ubicación donde se enviarán las imágenes de la cámara del robot
REF_TO_LIVE_IMAGE = ROOT_DB_REF.child('liveImages/' + config['robotUID'])

WALK_VELOCITY = 0.5


def move_joint(*args):
    """Función que se llama cuando se recibe una actualización que solicita
    mover una articulación.

    :param \*args: Se esperan dos argumentos, el nombre de la articulación y el ángulo que debe moverse

    :args[0]: (``str``) El nombre de la articulación
    :args[1]: (``float``) El valor en grados sexagesimales del ángulo.

    """
    try:
        joint_name = args[0]
        joint_angle = args[1]
    except IndexError:
        return
    try:
        ROBOT.move_joint(joint_name, float(joint_angle))
    except Exception as error:
        print(error)


def walk(*args):
    """Función que se llama cuando se recibe una actualización que solicita
    que el robot camine o se detenga.

    :param \*args: Se esperan dos argumentos, el primero debe ser nulo y el segundo la dirección a del caminado o la bandera de ``stop`` para que se detenga.

    :args[1]: (``str``) La dirección del caminado (forward, backward, right, left, turnLeft, turnRight, stop)

    """
    try:
        direction = args[1]
    except IndexError:
        return
    try:
        _x = 0
        _y = 0
        _theta = 0
        if direction == u'stop':
            ROBOT.stop_movement()
        if direction == u'forward':
            _x = WALK_VELOCITY
        if direction == u'backward':
            _x = -WALK_VELOCITY
        if direction == u'left':
            _y = WALK_VELOCITY
        if direction == u'right':
            _y = -WALK_VELOCITY
        if direction == u'turnLeft':
            _theta = WALK_VELOCITY
        if direction == u'turnRight':
            _theta = -WALK_VELOCITY
        ROBOT.move_robot(_x, _y, _theta)
    except Exception as error:
        print(error)


def change_posture(*args):
    """Función que se llama cuando se recibe una actualización que solicita
    el cambio de postura del robot.

    :param \*args: Se esperan dos argumentos, el primero debe ser nulo y el segundo la postura.

    :args[1]: (``str``) La postura del robot (Stand, Crouch)

    """
    try:
        posture = args[1]
    except IndexError:
        return
    try:
        ROBOT.change_posture(posture)
    except Exception as error:
        print(error)


def say(*args):
    """Función que se llama cuando se recibe una actualización que solicita
    que el robot diga un discurso.

    :param \*args: Se esperan dos argumentos, el primero debe ser nulo y el segundo el texto que debe repetir el robot.

    :args[1]: (``str``) El texto que debe decir el robot.

    """
    try:
        speech = args[1]
    except IndexError:
        return
    try:
        ROBOT.say_speech(speech)
    except Exception as error:
        print (error)


def set_velocity(*args):
    """Función que se llama cuando se recibe una actualización que solicita
    que el robot cambie la velocidad de caminado

    :param \*args: Se esperan dos argumentos, el primero debe ser nulo y el segundo la velocidad.

    :args[1]: (``float``) La velocidad de caminado (-1, 1)

    """
    global WALK_VELOCITY
    try:
        WALK_VELOCITY = float(args[1])
    except IndexError:
        return


def set_move_config(*args):
    """Función que se llama cuando se recibe una actualización que solicita
    que el robot cambie los parámetros de caminado

    :param \*args: Se esperan dos argumentos, el primero debe ser nulo y el segundo un diccionario con los parámetros que se quieren actualizar y los nuevos valores.

    :args[1]: (``dict``). Los nuevos valores para los parámetros de caminado

    """
    try:
        move_config = args[1]
    except IndexError:
        return
    try:
        ROBOT.set_move_config(move_config)
    except Exception as error:
        print (error)


#: El diccionario que mapea los valores recibidos de Firebase a una función que ejecute el comando.
ROBOT_OPTIONS = {
    u'moveJoint': move_joint,
    u'walk': walk,
    u'changePosture': change_posture,
    u'gaitParameters': set_move_config,
    u'speech': say,
    u'walkSpeed': set_velocity
}


def commands_handler(**kwargs):
    """La función del escuchador suscrito a la ubicación de Firebase
    ``REF_TO_COMMANDS``. Parse la respuesta y luego de acuerdo al comando enviado
    ejecuta la función que corresponde según ``ROBOT_OPTIONS``.
    """
    print(kwargs)
    commands_list = list(filter(None, kwargs['path'].strip().split('/')))
    if len(commands_list) > 0:
        command = commands_list[0]
        sub_command = commands_list[1] if len(commands_list) > 1 else None
        value = kwargs.get('data')
        try:
            ROBOT_OPTIONS[command](sub_command, value)
        except KeyError:
            print(KeyError)


def init_subscribers():
    """Inicializa los suscriptores. Por ahora sólo la cámara.
    """
    ROBOT.subscribe_to_camera()


def off_subscribers():
    """Da de baja los suscriptores.
    """
    ROBOT.unsubscribe_to_camera()


def run(robot_ip, robot_port):
    """La función que se llama al ejecutar la aplicación, inicializa la variable
    ``ROBOT`` como una instancia de la clase ``Robot()``. Se añade el agente que
    escucha los cambios en una referencia de la base de datos (``REF_TO_COMMANDS``).
    Inician los suscriptores. Luego con declara un bucle que se ejecuta
    hasta que el usuario interrumpe el programa. Mientras no se detenga
    envía constantes actualizaciones a Firebase. Cuando se detiene,
    se dan de baja los suscriptores y se elimina el escuchador.
    """
    print("Begin ...")
    global ROBOT
    ROBOT = Robot(robot_ip, robot_port)
    REF_TO_COMMANDS.add_event_listener(commands_handler)
    init_subscribers()
    REF_TO_STATUS.update({'network': 'online'})

    try:
        REF_TO_STATUS.set({'network': 'online'})
        while True:
            try:
                REF_TO_LOGS.push(ROBOT.get_values_from_memory())
                REF_TO_LIVE_IMAGE.set({'currentImage' : ROBOT.get_image_from_robot()})
                REF_TO_STATUS.update({'battery': ROBOT.get_battery_level()})
                time.sleep(3)
            except Exception as error:
                print (error)

    except KeyboardInterrupt:
        off_subscribers()
        REF_TO_STATUS.update({'network': 'offline'})
        REF_TO_COMMANDS.remove_event_listener()

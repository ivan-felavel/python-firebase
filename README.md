# Robot NAO + Firebase

Módulo para conectar al robot NAO a Firebase. Se utiliza para
recibir y enviar información a la base de datos de Firebase.
Recibe comandos de la aplicación web.

# Ejecución

```bash

python app.py --ip IP_DEL_ROBOT --port PUERTO_DEL_ROBOT

```

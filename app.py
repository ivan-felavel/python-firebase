# encoding:utf-8
"""
.. module:: app
   :platform: Unix, Windows
   :synopsis: Ejecuta la aplicación.

.. moduleauthor:: Ivan Feliciano <ivan.felavel@gmail.com>

"""

import argparse
from fire_nao import run


def main():
    """Inicia la aplicación, obtiene la dirección ip y puerto del robot a través
    de los argumentos enviados en la línea de comandos, luego llama a la Función
    ``run()`` del módulo ``fire_nao``.
    """
    parser = argparse.ArgumentParser(description="Una aplicación para conectar al robot NAO con Firebase")
    parser.add_argument('--ip', type=str, default='127.0.0.1', help="Dirección IP del robot")
    parser.add_argument('--port', type=int, default=9559, help="Puerto del robot")
    args = parser.parse_args()
    run(args.ip, args.port)

if __name__ == '__main__':
    main()

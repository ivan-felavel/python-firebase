# encoding:utf-8
"""
.. module:: firebase
   :platform: Unix, Windows
   :synopsis: Un módulo para interactuar con la API REST de Firebase Realtime Database

.. moduleauthor:: Ivan Feliciano <ivan.felavel@gmail.com>

"""
import json
import threading
import socket
import requests
from sseclient import SSEClient
from config import config


def check_response(response):
    """Función para verficar el código de estado de la respuesta enviada
    por firebase.

    :param response: El objeto con la respuesta enviada por Firebase.
    :return: Un diccionario con una respuesta de la petición.
    :type: dict
    """
    if response.status_code != 200:
        print("An error occurred {} with status code {}".format( \
            response.text, response.status_code))
    else:
        return response.json()


class FirebaseDatabase(object):
    """Una clase para usar utilizar la API REST de Firebase con métodos de alto nivel.
    Sirve para crear algo similar a las *Referencias* denifidas por Firebase.
    Un objeto de esta clase representa una ubicación específica en la base de datos.
    Con los métodos se realizan operaciones de escritura y lectura sobre esa ubicación.
    """
    auth = config['auth']
    params = {'auth': config['auth']}

    def __init__(self, url):
        """El constructor de la clase se encarga de definir la ubicación de la base de datos
        sobre la que se harán las operaciones.
        Necesita el diccionario ``config`` que contiene el valor del parámetro de autorización.
        """
        if not url.endswith('/'):
            url += '/'
        self.url = url

        self.remote_thread = None

    def get(self):
        """.. :method:: get()

        Este método obtiene los datos asociados con la ubicación del objeto a través
        del método ``GET`` del protocolo HTTP.
        """
        response = requests.get(self.url + '.json', params=self.params)
        return check_response(response)

    def child(self, path):
        """.. :method:: child(path)

        Se encarga de crear una nueva instancia con una ubicación relativa a la actual.
        
        :param path: La ruta que se añade a la ubicación actual.
        :return: Un nuevo objeto de ``FirebaseDatabase``
        """
        return FirebaseDatabase(self.url + path)

    def push(self, data):
        """.. :method:: push(data)

        Genera una nueva ubicación hija de la actual. Esta ubicación tiene un
        llave aleatoria única y se escriben los datos en esta.
        Por ejemplo, al hacer un ``push({"name" : "Rick Deckard"})`` a ``/users/``
        se produce ``/users/-L43aJQFQNzAIXry8_6g/name/`` con un valor ``Rick Deckard``.
        Simplemente hace un ``POST`` con los datos a enviar  a la URL del objeto.

        :param data: Los datos que se envían en la petición.
        :type data: Cualquier tipo de datos válido en un JSON.
        """

        data = json.dumps(data)
        response = requests.post(self.url + '.json', params=self.params, data=data)
        return check_response(response)

    def set(self, data):
        """ .. :method:: set(data)

        Escribe datos en la ubicación actual. Esto sobreescribe cualquier información
        que contenga.
        Hace un ``PUT`` sobre la ubicación actual.
        :param data: Los datos que se envían en la petición.
        :type data: Cualquier tipo de datos válido en un JSON.
        """
        data = json.dumps(data)
        response = requests.put(self.url + '.json', params=self.params, data=data)
        return check_response(response)

    def update(self, data):
        """ .. :method:: update(data)

        Escribe multiples valores en la ubicación actual de la base de datos. A diferencia
        de :meth:`set` solo actualiza los valores que se desean de acuerdo a la ubicación
        actual. Se hace un ``PATCH`` al URL.

        :param data: Los datos que se envían en la petición.
        :type data: Cualquier tipo de datos válido en un JSON.

        """
        data = json.dumps(data)
        response = requests.patch(self.url + '.json', params=self.params, data=data)
        return check_response(response)

    def remove(self):
        """ .. :method:: remove()

        Elimina todos los datos en la ubicación actual.
        """
        response = requests.delete(self.url + '.json', params=self.params)
        return check_response(response)

    def add_event_listener(self, handler_function):
        """ .. method:: add_event_listener(handler_function)

        Este método crea un hilo para escuchar los cambios en la ubicación actual
        de la base de datos, y manejar esos cambios en una función.

        :param handler_function: La función que maneja los eventos enviados por firebase.
        """
        self.remote_thread = RemoteThread(self.url + '.json?auth=' + self.auth, handler_function)
        self.remote_thread.start()

    def remove_event_listener(self):
        """ .. method:: remove_event_listener()

        Abandona el streaming de Firebase y une al hilo con el hilo principal.
        """
        if self.remote_thread:
            self.remote_thread.close()
            self.remote_thread.join()


class ClosableSSEClient(SSEClient):
    """Una clase pública desarrollada por el equipo de Firebase.
    Añade una funcionalidad al módulo ``SSEClient`` para
    abandonar correctamete el streaming.
    """

    def __init__(self, *args, **kwargs):
        self.should_connect = True
        super(ClosableSSEClient, self).__init__(*args, **kwargs)

    def _connect(self):
        """
        Realiza la conexión al instanciar al objecto.
        """
        if self.should_connect:
            super(ClosableSSEClient, self)._connect()
        else:
            raise StopIteration()

    def close(self):
        """
        Añade la funcionalidad necesaria para salir del streaming de manera correcta
        Se busca el socket dentro del módulo cliente de SSE y se cierra para lanzar una
        excepción y abandonar el streaming.
        """
        self.should_connect = False
        self.retry = 0
        # HACK: dig through the sseclient library to the requests library
        # down to the underlying socket.
        # then close that to raise an exception to get out of streaming.
        try:
            self.resp.raw._fp.fp._sock.shutdown(socket.SHUT_RDWR)
            self.resp.raw._fp.fp._sock.close()
        except AttributeError:
            pass


class RemoteThread(threading.Thread):
    """Una clase creada por el equipo de Firebase para que un hilo
    se encargue de los eventos enviados por Firebase, con una
    funcionalidad que añadí para que una función callback maneje
    los datos enviados por Firebase.
    """

    def __init__(self, url, handler):
        """
        """
        super(RemoteThread, self).__init__()
        self.url = url
        self.handler = handler

    def run(self):
        """ .. :method:: run()

        Método para recibir el streaming de Firebase hasta que se cierre la
        conexión.
        """
        try:
            self.sse = ClosableSSEClient(self.url)
            for msg in self.sse:
                msg_data = json.loads(msg.data)
                # print("Current msg_data")
                # print(msg_data)
                if msg_data is None:  # keep-alives
                    continue
                path = msg_data['path']
                data = msg_data['data']
                self.handler(data=data, path=path)
        except socket.error:
            print("error socket")
            pass  # this can happen when we close the stream

    def close(self):
        """ .. :method:: close()

        Deja el streaming, usando :meth:`close()` del cliente SSE.
        """
        if self.sse:
            self.sse.close()


# def print_data(*args):
#     """
#     Prueba de función manejadora
#     """
#     print("Estoy en el handler")
#     print(args[1])
#
#
# def main():
#     """
#     Prueba la clase FirebaseDatabase
#     """
#     firebase = FirebaseDatabase(config['databaseURL'])
#     user2 = firebase.child("other2")
#     user2.add_event_listener(print_data)
#     while True:
#         if raw_input() == 'stop':
#             break
#     user2.remove_event_listener()
#
#
# if __name__ == '__main__':
#     main()
